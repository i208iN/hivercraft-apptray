const { app, Menu, Tray, dialog } = require('electron');
const AutoLaunch = require('auto-launch');
const os = require('os');
const fs = require('fs');
const https = require('https');
const path = require('path');

const assetsPath = app.isPackaged ? path.join(process.resourcesPath, "assets") : "assets";
const userDataFolder = path.join(app.getPath('appData'), '.hivercraft');
const defaultMinecraftFolder = path.join(app.getPath('appData'), '.minecraft');
const configFile = path.join(userDataFolder, 'config.json');

let appConfig = {
    serverPacksFolder: null,
    updateDelay: 300,
    lastJobID: null,
    lastSync: 0
}

try {
    if (fs.existsSync(configFile)) {
        let json = JSON.parse(fs.readFileSync(configFile));
        if (json != null) {
            Object.keys(appConfig).forEach(key => {
                if (json[key] !== undefined) {
                    appConfig[key] = json[key];
                }
            });
        }
    }
} catch (e) { console.error(e); }

function saveConfig() {
    try {
        if (!fs.existsSync(userDataFolder)) fs.mkdirSync(userDataFolder);
        fs.writeFileSync(configFile, JSON.stringify(appConfig));
        updateMenu();
    }
    catch (e) { console.error(e) };
}


let autoLaunch = new AutoLaunch({
    name: "Hivercraft"
});

let autoLaunchEnabled = false;
let updateAutoLaunch = () => autoLaunch.isEnabled().then((value) => autoLaunchEnabled = value);
updateAutoLaunch();

function selectFolder() {
    dialog.showOpenDialog({
        title: "Select the \"server-resource-packs\" folder",
        properties: ['openDirectory'],
        defaultPath: appConfig.serverPacksFolder ? appConfig.serverPacksFolder : defaultMinecraftFolder
    })
        .then((result) => {
            if (!result.canceled) {
                let dir = result.filePaths[0];
                if (dir.indexOf('server-resource-packs') < 0) dir = path.join(dir, 'server-resource-packs');

                appConfig.serverPacksFolder = dir;
                saveConfig();
                console.log("New server resource packs folder:", appConfig.serverPacksFolder);
            }
        })
        .catch((reason) => console.error(reason));
}

function getLastSyncString() {
    let dt = Date.now() - appConfig.lastSync;
    dt = Math.floor(dt / 1000);
    if (dt < 120) return `${dt}s`;
    dt = Math.floor(dt / 60);
    if (dt < 60) return `${dt}m`;
    dt = Math.floor(dt / 24);
    if (dt > 3650) return '-'
    return `${dt}j`;
}

function updateMenu() {
    if (!mainTray) return;

    let delayOption = (label, delay) => { return { label: label, type: "checkbox", "checked": appConfig.updateDelay == delay, click: () => { appConfig.updateDelay = delay; saveConfig(); } } };
    mainTray.setContextMenu(Menu.buildFromTemplate([
        {
            label: "Hivercraft",
            icon: path.join(assetsPath, 'context.png'),
            enabled: false
        },
        {
            label: "Dernière vérification",
            sublabel: 'Il y a ' + getLastSyncString(),
            enabled: false
        },
        {
            type: 'separator'
        },
        {
            label: "Choisir dossier",
            sublabel: !appConfig.serverPacksFolder ? null : appConfig.serverPacksFolder,
            click: selectFolder
        },
        {
            label: "Synchroniser",
            click: synchronize,
            sublabel: !appConfig.serverPacksFolder ? '⚠ Aucun dossier' : null,
            enabled: appConfig.serverPacksFolder ? true : false
        },
        {
            type: 'separator'
        },
        {
            label: "Synchro auto",
            submenu: [
                delayOption("5 secondes", 5),
                delayOption("20 secondes", 20),
                delayOption("1 minute", 60),
                delayOption("5 minutes", 300),
                delayOption("15 minutes", 900),
                delayOption("60 minutes", 3600),
                delayOption("Manuel", -1),
            ]
        },
        {
            label: "Démarrage auto",
            submenu: [
                { label: "Oui", type: "checkbox", "checked": autoLaunchEnabled, click: () => { autoLaunch.enable(); updateAutoLaunch(); } },
                { label: "Non", type: "checkbox", "checked": !autoLaunchEnabled, click: () => { autoLaunch.disable(); updateAutoLaunch(); } },
            ]
        },
        {
            label: "Quitter",
            click: () => {
                app.quit();
            }
        }
    ]));
}

function handleResponse(response, filepath, jobID) {
    console.log(response);
    if (response.statusCode == 302 && response.headers['content-type'].indexOf('text/html') >= 0) {
        let location = response.headers['location'];
        if (location) {
            let match = (/-\/jobs\/(\d+)\/artifacts/g).exec(location);
            if (match) {
                jobID = match[1] || null;
                // If file deleted (or folder changed) or job has changed
                if (!fs.existsSync(filepath) || jobID != appConfig.lastJobID) {
                    https.get(location, (r) => handleResponse(r, filepath, jobID));
                } else {
                    console.log("Resource pack hasn't changed");
                }
            } else {
                https.get(location, (r) => handleResponse(r, filepath, jobID));
            }
        }
    }
    else if (response.statusCode == 200) {
        const filestream = fs.createWriteStream(filepath);
        response.pipe(filestream);
        appConfig.lastJobID = jobID;
        saveConfig();
    } else {
        console.error("Unexpected http status code", response.statusCode);
    }
}

function synchronize() {
    if (appConfig.serverPacksFolder) {
        if (!fs.existsSync(appConfig.serverPacksFolder)) {
            fs.mkdirSync(appConfig.serverPacksFolder);
        }
        let filepath = path.join(appConfig.serverPacksFolder, 'c780f439a17890ff823220759a9f91a756d61bc3');
        https.get("https://gitlab.com/i208iN/hivercraft-rp/-/jobs/artifacts/main/download?job=zip-pack", (r) => handleResponse(r, filepath, null));

        appConfig.lastSync = Date.now();
    } else {
        console.error('Cannot synchronize without the server resource pack folder.');
    }
}

let mainTray = null;
function registerTrayIcon() {
    const platform = os.platform();
    let trayImage;

    // Determine appropriate icon for platforms
    if (platform === 'darwin' || platform === 'linux') {
        trayImage = path.join(assetsPath, 'apptray.png');
    } else if (platform === 'win32') {
        trayImage = path.join(assetsPath, 'apptray.ico');
    }

    mainTray = new Tray(trayImage);
    mainTray.setToolTip('Hivercraft');
    // mainTray.on('click', selectFolder);
}

app.whenReady().then(() => {
    registerTrayIcon();
});

app.on("window-all-closed", event => {
    event.preventDefault();
});


let lastUpdate = null;

function Update() {
    if (appConfig.updateDelay >= 0) {
        if (lastUpdate == null || process.hrtime(lastUpdate)[0] >= appConfig.updateDelay) {
            synchronize();
            lastUpdate = process.hrtime();
        }
    }
    updateMenu();
    setTimeout(Update, 500);
}

Update();